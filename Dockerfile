FROM maven:3.8-jdk-11-slim

RUN groupadd --system --gid 1000 mavengroup && \
    adduser --disabled-password --gecos "" --force-badname --ingroup mavengroup rafaelperetta

RUN mkdir /app && \
    chown :1000 /app && \
    chmod 775 /app && \
    chmod g+s /app


USER rafaelperetta 