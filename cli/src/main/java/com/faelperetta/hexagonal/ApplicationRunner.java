package com.faelperetta.hexagonal;

import com.faelperetta.hexagonal.commands.ProductCommand;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.stereotype.Component;

import picocli.CommandLine;
import picocli.CommandLine.IFactory;

@Component
public class ApplicationRunner implements CommandLineRunner, ExitCodeGenerator {

    private final ProductCommand productCommand;

    private final IFactory factory;

    private int exitCode;

    public ApplicationRunner(ProductCommand myCommand, IFactory factory) {
        this.productCommand = myCommand;
        this.factory = factory;
    }

    @Override
    public void run(String... args) throws Exception {
        exitCode = new CommandLine(productCommand, factory).execute(args);
    }

    @Override
    public int getExitCode() {
        return exitCode;
    }
}