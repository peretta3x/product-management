package com.faelperetta.hexagonal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CliMain {

    public static void main(String[] args) {
        System.exit(SpringApplication.exit(SpringApplication.run(CliMain.class, args)));
    }
}