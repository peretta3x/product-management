package com.faelperetta.hexagonal.commands;

import java.math.BigDecimal;
import java.util.concurrent.Callable;

import com.faelperetta.hexagonal.application.Product;
import com.faelperetta.hexagonal.application.ProductService;

import org.springframework.stereotype.Component;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Component
@Command(name = "create", mixinStandardHelpOptions = true, description = "Creates a new product")
public class CreateCommand implements Callable<Integer> {

    private final ProductService productService;

    public CreateCommand(ProductService productService) {
        this.productService = productService;
    }

    @Option(names = "--name", description = "Product name")
    String name;

    @Option(names = "--price", description = "Product price")
    BigDecimal price;

    @Override
    public Integer call() {
        Product createdProduct = this.productService.create(name, price);
        System.out.println("Product has been created");
        System.out.printf("ID: %s\n", createdProduct.getId());
        System.out.printf("Name: %s\n", createdProduct.getName());
        System.out.printf("Price: %s\n", createdProduct.getPrice());
        System.out.printf("Status: %s\n", createdProduct.getStatus());
        return 0;
    }
}
