package com.faelperetta.hexagonal.commands;

import java.util.concurrent.Callable;

import com.faelperetta.hexagonal.application.ProductService;

import org.springframework.stereotype.Component;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Component
@Command(name = "disable", mixinStandardHelpOptions = true, description = "Disable product")
public class DisableCommand implements Callable<Integer> {

    private final ProductService productService;

    @Option(names = "--id", description = "Product id")
    String id;

    public DisableCommand(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public Integer call() {
        try {
            var product = productService.disable(productService.get(id));
            System.out.printf("Product %s is disabled\n", product.getName());
        } catch (RuntimeException ex) {
            System.out.printf("Product with id=%s no found.\n", this.id);
            return CommandLine.ExitCode.USAGE;
        }
        return CommandLine.ExitCode.OK;
    }

}
