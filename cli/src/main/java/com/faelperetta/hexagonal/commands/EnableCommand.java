package com.faelperetta.hexagonal.commands;

import java.util.concurrent.Callable;

import com.faelperetta.hexagonal.application.ProductService;

import org.springframework.stereotype.Component;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Component
@Command(name = "enable", mixinStandardHelpOptions = true, description = "Enable product")
public class EnableCommand implements Callable<Integer> {

    private final ProductService productService;

    @Option(names = "--id", description = "Product id")
    String id;

    public EnableCommand(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public Integer call() {
        try {
            var product = productService.enable(productService.get(id));
            System.out.printf("Product %s is enabled\n", product.getName());
        } catch (RuntimeException ex) {
            System.out.printf("Product with id=%s no found.\n", this.id);
            return CommandLine.ExitCode.USAGE;
        }
        return CommandLine.ExitCode.OK;
    }

}
