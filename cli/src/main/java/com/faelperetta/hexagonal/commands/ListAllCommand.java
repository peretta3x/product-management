package com.faelperetta.hexagonal.commands;

import java.util.List;

import com.faelperetta.hexagonal.application.Product;
import com.faelperetta.hexagonal.application.ProductService;

import org.springframework.stereotype.Component;

import picocli.CommandLine.Command;

@Component
@Command(name = "list", mixinStandardHelpOptions = true, description = "List all products")
public class ListAllCommand implements Runnable {

    private final ProductService productService;

    public ListAllCommand(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public void run() {
        List<Product> products = this.productService.getAll();
        products.stream()
                .map(p -> String.format("ID: %s, Name: %s, Price %s, Status: %s", p.getId(), p.getName(), p.getPrice(),
                        p.getStatus()))
                .forEach(System.out::println);
        System.out.printf("Total of %s products.\n", products.size());
    }

}
