package com.faelperetta.hexagonal.commands;

import java.util.concurrent.Callable;

import org.springframework.context.annotation.Configuration;

import picocli.CommandLine;
import picocli.CommandLine.Command;

@Configuration
@Command(name = "commandParse", mixinStandardHelpOptions = true, description = "Products Management", subcommands = {
        CreateCommand.class, ListAllCommand.class, EnableCommand.class, DisableCommand.class })
public class ProductCommand implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        System.out.println("Product Management");
        return CommandLine.ExitCode.OK;
    }

}
