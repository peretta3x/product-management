package com.faelperetta.hexagonal.configuration;

import com.faelperetta.hexagonal.application.ProductPersistence;
import com.faelperetta.hexagonal.application.ProductService;
import com.faelperetta.hexagonal.application.ProductServiceImpl;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CliConfiguration {

    @Bean
    public ProductService createProductService(ProductPersistence productPersistence) {
        return new ProductServiceImpl(productPersistence);
    }

}
