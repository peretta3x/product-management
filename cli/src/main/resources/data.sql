DROP TABLE IF EXISTS products;

CREATE TABLE products (
  id VARCHAR(250)  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  price DECIMAL NOT NULL,
  status VARCHAR(25) DEFAULT NULL
);

INSERT INTO products  VALUES ('1', 'Product 1', 10.0, 'disabled');
INSERT INTO products  VALUES ('2', 'Product 2', 55.0, 'disabled');
