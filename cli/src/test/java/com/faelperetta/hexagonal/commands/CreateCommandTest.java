package com.faelperetta.hexagonal.commands;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import com.faelperetta.hexagonal.application.Product;
import com.faelperetta.hexagonal.application.ProductService;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import picocli.CommandLine;

class CreateCommandTest {

    private final ProductService productService = Mockito.mock(ProductService.class);
    private final CreateCommand createCommand = new CreateCommand(productService);

    @Test
    void shouldCreateNewProduct() {
        var product = new Product("1", "Product 1", BigDecimal.ONE, "disabled");
        when(productService.create("Product 1", BigDecimal.ONE)).thenReturn(product);

        createCommand.name = "Product 1";
        createCommand.price = BigDecimal.ONE;
        var result = createCommand.call();

        verify(productService).create("Product 1", BigDecimal.ONE);
        assertThat(result).isEqualTo(CommandLine.ExitCode.OK);
    }

}
