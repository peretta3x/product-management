package com.faelperetta.hexagonal.commands;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import com.faelperetta.hexagonal.application.Product;
import com.faelperetta.hexagonal.application.ProductService;

import org.junit.jupiter.api.Test;

import picocli.CommandLine;

class DisableCommandTest {

    private final ProductService productService = mock(ProductService.class);
    private final DisableCommand disableCommand = new DisableCommand(productService);

    @Test
    void shouldDisableProduct() {
        var product = new Product("1", "Product 1", BigDecimal.ONE, "enabled");
        var productEnabled = new Product("1", "Product 1", BigDecimal.ONE, "disabled");

        when(productService.get("1")).thenReturn(product);
        when(productService.disable(product)).thenReturn(productEnabled);

        disableCommand.id = "1";
        int result = disableCommand.call();

        verify(productService).get("1");
        verify(productService).disable(product);

        assertThat(result).isEqualTo(CommandLine.ExitCode.OK);
    }

    @Test
    void shouldFailToDisableProduct() {
        when(productService.get("1")).thenThrow(RuntimeException.class);

        disableCommand.id = "1";
        int result = disableCommand.call();

        verify(productService).get("1");
        assertThat(result).isEqualTo(CommandLine.ExitCode.USAGE);
    }

}
