package com.faelperetta.hexagonal.commands;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import com.faelperetta.hexagonal.application.Product;
import com.faelperetta.hexagonal.application.ProductService;

import org.junit.jupiter.api.Test;

import picocli.CommandLine;

class EnableCommandTest {

    private final ProductService productService = mock(ProductService.class);
    private final EnableCommand enableCommand = new EnableCommand(productService);

    @Test
    void shouldEnableProduct() {
        var product = new Product("1", "Product 1", BigDecimal.ONE, "disabled");
        var productEnabled = new Product("1", "Product 1", BigDecimal.ONE, "enabled");

        when(productService.get("1")).thenReturn(product);
        when(productService.enable(product)).thenReturn(productEnabled);

        enableCommand.id = "1";
        enableCommand.call();

        verify(productService).get("1");
        verify(productService).enable(product);

        assertThat(productEnabled.getStatus()).isEqualTo("enabled");
    }

    @Test
    void shouldFailToEnableProduct() {
        when(productService.get("1")).thenThrow(RuntimeException.class);

        enableCommand.id = "1";
        var result = enableCommand.call();

        verify(productService).get("1");
        assertThat(result).isEqualTo(CommandLine.ExitCode.USAGE);
    }

}
