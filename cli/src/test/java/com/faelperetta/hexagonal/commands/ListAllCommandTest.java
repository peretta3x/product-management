package com.faelperetta.hexagonal.commands;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.List;

import com.faelperetta.hexagonal.application.Product;
import com.faelperetta.hexagonal.application.ProductService;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class ListAllCommandTest {

    private final ProductService productService = Mockito.mock(ProductService.class);
    private final ListAllCommand listAllCommand = new ListAllCommand(productService);

    @Test
    void shouldCreateNewProduct() {
        var product = new Product("1", "Product 1", BigDecimal.ONE, "disabled");
        when(productService.getAll()).thenReturn(List.of(product));

        listAllCommand.run();

        verify(productService).getAll();
    }

}
