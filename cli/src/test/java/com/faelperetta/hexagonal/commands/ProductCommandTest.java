package com.faelperetta.hexagonal.commands;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import picocli.CommandLine;

class ProductCommandTest {

    @Test
    void shouldProdutCommandSuccessfuly() throws Exception {
        var productCommand = new ProductCommand();
        var result = productCommand.call();
        assertThat(result).isEqualTo(CommandLine.ExitCode.OK);
    }

}
