package com.faelperetta.hexagonal.configuration;

import com.faelperetta.hexagonal.application.ProductPersistence;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class CliConfigurationTest {

    @Test
    void shouldCreateProductServiceBean() {
        var productPersistence = Mockito.mock(ProductPersistence.class);
        var cliConfiguration = new CliConfiguration();
        var productService = cliConfiguration.createProductService(productPersistence);
        Assertions.assertThat(productService).isNotNull();
    }

}
