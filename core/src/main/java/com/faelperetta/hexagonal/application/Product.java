package com.faelperetta.hexagonal.application;

import static java.util.Objects.requireNonNull;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

public class Product {

    private String id;
    private String name;
    private BigDecimal price;
    private String status;

    public Product(String id, String name, BigDecimal price, String status) {
        this.id = requireNonNull(id, "id cannot be null");
        this.name = requireNonNull(name, "name cannot be null");

        if (Objects.nonNull(price)) {
            if (price.compareTo(BigDecimal.ZERO) < 0) {
                throw new IllegalArgumentException("price must be greater or equal zero");
            }
            this.price = price;
        } else {
            this.price = BigDecimal.ZERO;
        }

        this.status = Optional.ofNullable(status).orElse("disabled");
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getStatus() {
        return status;
    }

    public void enable() {
        if (this.price.compareTo(BigDecimal.ZERO) > 0) {
            this.status = "enabled";
            return;
        }
        throw new IllegalStateException("The price must be greater than zero to enable the product");
    }

    public void disable() {
        if (this.price.compareTo(BigDecimal.ZERO) == 0) {
            this.status = "disabled";
            return;
        }
        throw new IllegalStateException("The price must be zero in order to have the product disabled");
    }
}
