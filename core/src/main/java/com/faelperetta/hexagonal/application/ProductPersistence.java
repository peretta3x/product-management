package com.faelperetta.hexagonal.application;

import java.util.List;
import java.util.Optional;

public interface ProductPersistence {

    Product save(Product product);

    Optional<Product> get(String id);

    List<Product> getAll();

}
