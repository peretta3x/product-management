package com.faelperetta.hexagonal.application;

import java.math.BigDecimal;
import java.util.List;

public interface ProductService {
    Product get(String id);

    Product create(String name, BigDecimal price);

    Product enable(Product product);

    Product disable(Product product);

    List<Product> getAll();
}
