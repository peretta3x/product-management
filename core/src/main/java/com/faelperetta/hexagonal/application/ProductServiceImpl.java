package com.faelperetta.hexagonal.application;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class ProductServiceImpl implements ProductService {

    private ProductPersistence persistence;

    public ProductServiceImpl(ProductPersistence persistence) {
        this.persistence = persistence;
    }

    @Override
    public Product get(String id) {
        return persistence.get(id).orElseThrow(() -> new RuntimeException("The product does not exists"));
    }

    @Override
    public Product create(String name, BigDecimal price) {
        UUID uuid = UUID.randomUUID();
        var product = new Product(uuid.toString(), name, price, null);
        return persistence.save(product);
    }

    @Override
    public Product enable(Product product) {
        product.enable();
        return persistence.save(product);
    }

    @Override
    public Product disable(Product product) {
        product.disable();
        return persistence.save(product);
    }

    @Override
    public List<Product> getAll() {
        return this.persistence.getAll();
    }
}
