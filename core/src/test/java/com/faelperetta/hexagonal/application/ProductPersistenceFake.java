package com.faelperetta.hexagonal.application;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProductPersistenceFake implements ProductPersistence {

    private Map<String, Product> products = new HashMap<>();

    @Override
    public Product save(Product product) {
        products.put(product.getId(), product);
        return product;
    }

    @Override
    public Optional<Product> get(String id) {
        return Optional.ofNullable(products.get(id));
    }

    @Override
    public List<Product> getAll() {
        return this.products.values().stream().collect(Collectors.toList());
    }
}
