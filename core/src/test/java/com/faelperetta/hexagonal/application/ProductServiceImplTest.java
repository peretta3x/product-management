package com.faelperetta.hexagonal.application;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ProductServiceImplTest {

    private ProductPersistence persistence = new ProductPersistenceFake();
    private ProductService productService = new ProductServiceImpl(persistence);

    @Test
    void shouldGetProductById() {
        var product = new Product(UUID.randomUUID().toString(), "Product 1", new BigDecimal(1), null);
        persistence.save(product);

        var productFound = productService.get(product.getId());
        assertThat(productFound).extracting("id", "name", "price", "status").containsExactly(product.getId(),
                product.getName(), product.getPrice(), product.getStatus());
    }

    @Test
    void shouldThrowExceptionWhenProductIsNotFound() {
        var product = new Product(UUID.randomUUID().toString(), "Product 1", new BigDecimal(1), null);
        persistence.save(product);

        assertThatThrownBy(() -> productService.get("id")).isInstanceOf(RuntimeException.class)
                .hasMessage("The product does not exists");
    }

    @Test
    void shouldCreateANewProduct() {
        var product = productService.create("Product 2", new BigDecimal(30));
        assertThat(product).extracting("name", "price", "status").containsExactly("Product 2", new BigDecimal(30),
                "disabled");
    }

    @Test
    void shouldEnableAProduct() {
        var product = new Product(UUID.randomUUID().toString(), "Product 1", new BigDecimal(1), null);
        persistence.save(product);
        productService.enable(product);
        assertThat(product.getStatus()).isEqualTo("enabled");
    }

    @Test
    void shouldDisableAProduct() {
        var product = new Product(UUID.randomUUID().toString(), "Product 1", BigDecimal.ZERO, "enabled");
        productService.disable(product);
        assertThat(product.getStatus()).isEqualTo("disabled");
    }

    @Test
    void shouldReturnAnEmptyListOfProducts() {
        var productList = this.productService.getAll();
        assertThat(productList).isEmpty();
    }

    @Test
    void shouldReturnAnTwoProductOnTheList() {
        var product1 = new Product(UUID.randomUUID().toString(), "Product 1", new BigDecimal(1), null);
        persistence.save(product1);

        var product2 = new Product(UUID.randomUUID().toString(), "Product 2", new BigDecimal(1), null);
        persistence.save(product2);

        var productList = this.productService.getAll();
        assertThat(productList).hasSize(2);
    }
}