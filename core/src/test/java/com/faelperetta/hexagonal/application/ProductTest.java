package com.faelperetta.hexagonal.application;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ProductTest {

    @Test
    void shouldCreateNewProduct() {
        var product = new Product("xpto", "Product 1", new BigDecimal(10.0), "enabled");
        assertThat(product.getId()).isEqualTo("xpto");
        assertThat(product.getName()).isEqualTo("Product 1");
        assertThat(product.getPrice()).isEqualTo(new BigDecimal(10.0));
        assertThat(product.getStatus()).isEqualTo("enabled");
    }

    @Test
    void shouldCreateNewProductWithStatusDisabled() {
        var product = new Product("xpto", "Product 1", new BigDecimal(10.0), null);
        assertThat(product.getId()).isEqualTo("xpto");
        assertThat(product.getName()).isEqualTo("Product 1");
        assertThat(product.getPrice()).isEqualTo(new BigDecimal(10.0));
        assertThat(product.getStatus()).isEqualTo("disabled");
    }

    @Test
    void shouldCreateNewProductWithPriceZero() {
        var product = new Product("xpto", "Product 1", null, null);
        assertThat(product.getId()).isEqualTo("xpto");
        assertThat(product.getName()).isEqualTo("Product 1");
        assertThat(product.getPrice()).isEqualTo(BigDecimal.ZERO);
        assertThat(product.getStatus()).isEqualTo("disabled");
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenPriceIsLessThanZero() {
        assertThatThrownBy(
                () -> new Product("xpto", "Product 1", new BigDecimal(-1), null)
        ).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void shouldThrowNullPointerExceptionWhenPassIdAsNull() {
        assertThatThrownBy(
                () -> new Product(null, "Product 1", new BigDecimal(10.0), null)
        ).isInstanceOf(NullPointerException.class);
    }

    @Test
    void shouldThrowNullPointerExceptionWhenPassNameAsNull() {
        assertThatThrownBy(
                () -> new Product("xpto", null, new BigDecimal(10.0), null)
        ).isInstanceOf(NullPointerException.class);
    }

    @Test
    void shouldEnableTheProduct() {
        var product = new Product("xpto", "Product 1", new BigDecimal(10.0), "disabled");
        product.enable();
        assertThat(product.getStatus()).isEqualTo("enabled");
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenEnableProductWithPriceLessThanOne() {
        var product = new Product("xpto", "Product 1", BigDecimal.ZERO, "disabled");
        assertThatThrownBy(() -> product.enable()).
                isInstanceOf(IllegalStateException.class)
                .hasMessage("The price must be greater than zero to enable the product");
    }

    @Test
    void shouldDisableTheProduct() {
        var product = new Product("xpto", "Product 1", BigDecimal.ZERO, "enabled");
        product.disable();
        assertThat(product.getStatus()).isEqualTo("disabled");
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenDisableProductWithPriceLessThanOne() {
        var product = new Product("xpto", "Product 1", BigDecimal.ONE, "enabled");
        assertThatThrownBy(() -> product.disable()).
                isInstanceOf(IllegalStateException.class)
                .hasMessage("The price must be zero in order to have the product disabled");
    }

}
