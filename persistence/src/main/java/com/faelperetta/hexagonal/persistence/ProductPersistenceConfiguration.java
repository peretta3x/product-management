package com.faelperetta.hexagonal.persistence;

import com.faelperetta.hexagonal.application.ProductPersistence;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProductPersistenceConfiguration {

    @Bean
    public ProductPersistence productPersistence(ProductRepository productRepository) {
        return new ProductPersistenceImpl(productRepository);
    }
}
