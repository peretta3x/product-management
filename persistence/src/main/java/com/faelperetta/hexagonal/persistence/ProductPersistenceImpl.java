package com.faelperetta.hexagonal.persistence;

import com.faelperetta.hexagonal.application.Product;
import com.faelperetta.hexagonal.application.ProductPersistence;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProductPersistenceImpl implements ProductPersistence {

    private ProductRepository repository;

    public ProductPersistenceImpl(ProductRepository repository) {
        this.repository = repository;
    }

    @Override
    public Product save(Product product) {
        var productEntity = new ProductEntity(product.getId(), product.getName(), product.getPrice(),
                product.getStatus());
        this.repository.save(productEntity);
        return product;
    }

    @Override
    public Optional<Product> get(String id) {
        return this.repository.findById(id)
                .map(entity -> new Product(entity.getId(), entity.getName(), entity.getPrice(), entity.getStatus()));
    }

    @Override
    public List<Product> getAll() {
        return this.repository.findAll().stream()
                .map(entity -> new Product(entity.getId(), entity.getName(), entity.getPrice(), entity.getStatus()))
                .collect(Collectors.toList());
    }
}
