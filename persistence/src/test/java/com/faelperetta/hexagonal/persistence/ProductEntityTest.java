package com.faelperetta.hexagonal.persistence;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class ProductEntityTest {

    @Test
    void shouldCreateNewProductEntity() {
        var productEntity = new ProductEntity();
        assertThat(productEntity).isNotNull();
    }
    
}
