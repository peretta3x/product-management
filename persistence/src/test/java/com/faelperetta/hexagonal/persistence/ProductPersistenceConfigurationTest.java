package com.faelperetta.hexagonal.persistence;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class ProductPersistenceConfigurationTest {

    private ProductPersistenceConfiguration productPersistenceConfiguration = new ProductPersistenceConfiguration();

    @Test
    void shouldCreateNewProductPersistenceBean() {
        var productPersistence = productPersistenceConfiguration
            .productPersistence(Mockito.mock(ProductRepository.class));
        Assertions.assertNotNull(productPersistence);
    }
    
}
