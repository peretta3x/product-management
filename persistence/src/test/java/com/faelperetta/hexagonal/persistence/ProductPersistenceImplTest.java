package com.faelperetta.hexagonal.persistence;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import com.faelperetta.hexagonal.application.Product;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class ProductPersistenceImplTest {

    private ProductRepository repository = mock(ProductRepository.class);
    private ProductPersistenceImpl productPersistenceImpl = new ProductPersistenceImpl(repository);

    @Test
    void shouldGetAllProducts() {
        ProductEntity productEntity = new ProductEntity("1", "Product 1", BigDecimal.ONE, "enabled");
        when(repository.findAll()).thenReturn(List.of(productEntity));

        var products = this.productPersistenceImpl.getAll();

        assertThat(products).hasSize(1);
        assertThat(products).element(0).extracting("name").isEqualTo("Product 1");
    }

    @Test
    void shouldGetProductById() {
        ProductEntity productEntity = new ProductEntity("1", "Product 1", BigDecimal.ONE, "enabled");
        when(repository.findById("1")).thenReturn(Optional.of(productEntity));

        var product = this.productPersistenceImpl.get("1");

        assertTrue(product.isPresent());
        assertThat(product.get()).extracting("name").isEqualTo("Product 1");
    }

    @Test
    void shouldSaveProduct() {
        Product newProduct = new Product("1", "Product 1", BigDecimal.ONE, "enabled");
        ProductEntity productEntity = new ProductEntity("1", "Product 1", BigDecimal.ONE, "enabled");
        when(repository.save(Mockito.any(ProductEntity.class))).thenReturn(productEntity);

        var product = this.productPersistenceImpl.save(newProduct);

        Assertions.assertNotNull(product);
        assertThat(product).extracting("name").isEqualTo("Product 1");
    }
    
}
