package com.faelperetta.hexagonal.controllers;

import java.util.List;

import com.faelperetta.hexagonal.application.Product;
import com.faelperetta.hexagonal.application.ProductService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("")
    public ResponseEntity<Product> create(@RequestBody ProductRequest productRequest) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(productService.create(productRequest.getName(), productRequest.getPrice()));
    }

    @GetMapping("")
    public ResponseEntity<List<Product>> getAll() {
        return ResponseEntity.ok(this.productService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> get(@PathVariable("id") String id) {
        return ResponseEntity.ok(productService.get(id));
    }

    @PutMapping("/{id}/enable")
    public ResponseEntity<Product> enable(@PathVariable("id") String id) {
        return ResponseEntity.ok(productService.enable(productService.get(id)));
    }

    @PutMapping("/{id}/disable")
    public ResponseEntity<Product> disable(@PathVariable("id") String id) {
        return ResponseEntity.ok(productService.disable(productService.get(id)));
    }
}
