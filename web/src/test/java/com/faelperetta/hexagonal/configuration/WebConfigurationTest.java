package com.faelperetta.hexagonal.configuration;

import com.faelperetta.hexagonal.application.ProductPersistence;
import com.faelperetta.hexagonal.application.ProductServiceImpl;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class WebConfigurationTest {

    private WebConfiguration webConfiguration = new WebConfiguration();

    @Test
    void shouldCreateNewPersistenceBean() {
        var persistenceBean = this.webConfiguration.productService(Mockito.mock(ProductPersistence.class));
        Assertions.assertThat(persistenceBean).isInstanceOf(ProductServiceImpl.class);
    }
    
}
