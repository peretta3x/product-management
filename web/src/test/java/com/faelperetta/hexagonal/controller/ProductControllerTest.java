package com.faelperetta.hexagonal.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.List;

import com.faelperetta.hexagonal.application.Product;
import com.faelperetta.hexagonal.application.ProductService;
import com.faelperetta.hexagonal.controllers.ProductController;
import com.faelperetta.hexagonal.controllers.ProductRequest;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class ProductControllerTest {


    private final ProductService productService = Mockito.mock(ProductService.class);

    private final ProductController productController = new ProductController(productService);

    @Test
    void shouldGetProductById() {
        when(productService.get("1")).thenReturn(new Product("1", "Product 1", new BigDecimal(1), "enabled"));
        
        var response = productController.get("1");
        var product = response.getBody();

        verify(productService).get("1");
        Assertions.assertThat(product.getId()).isEqualTo("1");
    }

    @Test
    void shouldEnableProductById() {
        var product = new Product("1", "Product 1", new BigDecimal(1), "enabled");
        when(productService.get("1")).thenReturn(product);
        when(productService.enable(product)).thenReturn(product);
        
        productController.enable("1");
        
        verify(productService).get("1");
        verify(productService).enable(product);
    }

    @Test
    void shouldDisableProductById() {
        var product = new Product("1", "Product 1", new BigDecimal(1), "enabled");
        when(productService.get("1")).thenReturn(product);
        when(productService.disable(product)).thenReturn(product);
        
        productController.disable("1");
        
        verify(productService).get("1");
        verify(productService).disable(product);
    }

    @Test
    void shouldGetAllProducts() {
        var product = new Product("1", "Product 1", new BigDecimal(1), "enabled");
        when(productService.getAll()).thenReturn(List.of(product));    
        productController.getAll();
        verify(productService).getAll();
    }

    @Test
    void shouldCreateNewProduct() {
        var productRequest = new ProductRequest("Product Test", BigDecimal.ONE);
        ResponseEntity<Product> createResponse = productController.create(productRequest);
        assertThat(createResponse.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        verify(productService).create(productRequest.getName(), productRequest.getPrice());
    }
    
}
