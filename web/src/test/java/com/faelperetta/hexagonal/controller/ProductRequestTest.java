package com.faelperetta.hexagonal.controller;

import static org.assertj.core.api.Assertions.assertThat;

import com.faelperetta.hexagonal.controllers.ProductRequest;

import org.junit.jupiter.api.Test;

class ProductRequestTest {

    @Test
    void shouldCreateNewProductRequestUsingDefaultConstructor() {
       var request = new ProductRequest();
       assertThat(request).isNotNull();
    }
    
}
